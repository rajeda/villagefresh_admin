import React,{useEffect,useState} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";

const CategoriesList = () => {

    const [categories, setCategories] = useState([]);

    useEffect(  () => {

        async function callAjax() {
            var response = await loadContent();
            response = response.data;
            if(response.status === 1){
                setCategories(response.data);
            }
        }

        callAjax();


    }, [])

    const loadContent = () => {
        return  axios.get(SERVER_URL+"categories");
    }

    const removeCategory = async (cat_id) => {

        var confirm =window.confirm("Are you sure wanted to remove it ? . Please make sure to remove all subcategories associates with category.");

        if(confirm){

            var remove_category = await axios.delete(`${SERVER_URL}categories/${cat_id}`);
            if(remove_category){
                remove_category = remove_category.data;
                if(remove_category.status === 1){
                    alert("successfully deleted");
                }
            }
        }

    }

    return(
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Category Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    categories && categories.map( (val,index) => {
                        return(
                            <tr key={index}>
                                <td>{index+1}</td>
                                <td>{val.category_name}</td>
                                <td>
                                <Link className="btn btn-secondary" to={`/categories/list/edit/${val._id}`} >Edit</Link>
                                <button className="ml-2" onClick={
                                    (e) => {
                                        removeCategory(val._id);
                                    }
                                }>Delete</button>
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
    )
};


export default CategoriesList;


