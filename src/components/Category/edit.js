import Axios from "axios";
import React , { useEffect , useState}from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "../../config";


const EditCategoryForm = () => {

    const { cat_id } = useParams();
    const [category,setCategory] = useState("");

    const updateCategory = async (e) => {

        e.preventDefault();

        const formdata = {
            cat_id : cat_id,
            category_name : category
        };

        var category_update = await axios.put(`${SERVER_URL}categories`,formdata);
        if(category_update){
            category_update = category_update.data;
            if(category_update.status === 1) {
                alert("Successfully updated");
            } else{
                alert("Failed to updated");
            }      
        }
    };

    useEffect(
        () => {
            const callAjax = async () => {
                var category = await axios.get(`${SERVER_URL}categories/${cat_id}`);
                if(category){
                    category = category.data;
                    if(category.status === 1) {
                        setCategory(category.data.category_name);
                    }
                }
            }

            callAjax();
        },
        []
    )
    return(

        <div>
        <div className="form-group">
            <label>Category Name:</label>
            <input type="text" name="category_name" id="category_name" value={category}
                onChange={(e) => {
                    setCategory(e.target.value);
                }}
            />
        </div>

        <button className="btn btn-primary" onClick={updateCategory}>Save</button>
    </div>
    )
};

export default EditCategoryForm;