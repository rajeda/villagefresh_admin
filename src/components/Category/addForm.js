import React,{useState} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";

const AddForm = () => {

    const [category,setCategory] = useState("");

    const updateCategoryInputValue = (e) => {
        setCategory(e.target.value);
    };

    const createCategory = () => {

        if(category === "") {
            alert("Category name requried");
            return;
        }
        let url = SERVER_URL+"categories";
        let formdata = {category_name : category };
        axios.post(url,formdata)
            .then((res) => {
                console.log("Res",res);
                alert("Succfully added");
            })
            .catch(err => {
                console.log("err",err);
            });


    };

    return(

        <div>
            <div className="form-group">
                <label>Category Name:</label>
                <input type="text" name="category_name" id="category_name" value={category}
                    onChange={updateCategoryInputValue}
                />
            </div>

            <button className="btn btn-primary" onClick={createCategory}>Save</button>
        </div>
    )
}

export default AddForm;