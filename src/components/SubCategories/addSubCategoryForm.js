import React,{useState,useEffect} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";

const AddSubCategoryForm = () => {

    const [category,setCategory] = useState([]);
    const [subcategory,setsubCategory] = useState("");
    const [updated_category,setUpdateCategory] = useState("");

    const updateSubCategoryInputValue = (e) => {
        setsubCategory(e.target.value);
    };

    const updateCategoryChangeValue = (e) => {
        setUpdateCategory(e.target.value);
    };
    const createSubCategory = () => {

        if(updated_category === "") {
            alert("Category requried");
            return;
        }
        if(subcategory === "") {
            alert("Sub Category name requried");
            return;
        }

        let url = SERVER_URL+"sub-categories";
        let formdata = {sub_category_name : subcategory , category : updated_category };
        axios.post(url,formdata)
            .then((res) => {
                console.log("Res",res);
                alert("Succfully added");
            })
            .catch(err => {
                console.log("err",err);
            });


    };

    useEffect(() => {


        async function callAjax() {
            var response = await loadCategories();
            response = response.data;
            console.log(response)

            if(response.status === 1){
                setCategory(response.data);
            }
        }

        callAjax();

    },[]);

    const loadCategories = () => {
        return  axios.get(SERVER_URL+"categories");
    }

    return(

        <div>
            <div className="form-group">
                <label>Category:</label>
                <select className="form-control" onChange={updateCategoryChangeValue}>
                    <option>Select Category</option>
                    {
                        category && category.map((val,index) => {
                            return <option key={index} value={val._id}>{val.category_name}</option>
                        })
                    }
                </select>
            </div>
            <div className="form-group">
                <label>Sub Category Name:</label>
                <input type="text" name="category_name"  className="form-control" id="category_name" value={subcategory}
                    onChange={updateSubCategoryInputValue}
                />
            </div>

            <button className="btn btn-primary" onClick={createSubCategory}>Save</button>
        </div>
    )
}

export default AddSubCategoryForm;