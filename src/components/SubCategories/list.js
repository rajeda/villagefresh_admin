import React,{useEffect,useState} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";


const SubCategoriesList = () => {

    const [subcategories, setSubCategories] = useState([]);

    useEffect(  () => {



        callAjax();


    }, [])

    async function callAjax() {
        var response = await loadContent();
        response = response.data;
        if(response.status === 1){
            setSubCategories(response.data);
        }
    }

    const loadContent = () => {
        return  axios.get(SERVER_URL+"sub-categories");
    }

    const remove = async (id) => {

        var confirm = window.confirm("Are you sure wanted to remove?");

        if(confirm) {

            var remove_subcategory = await axios.delete(`${SERVER_URL}sub-categories/${id}`);
            remove_subcategory = remove_subcategory.data;
            if(remove_subcategory.status === 1){
                callAjax();
            }
        }
    }
    return(
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Sub Category Name</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    subcategories && subcategories.map( (val,index) => {
                        return(
                            <tr key={index}>
                                <td>{index+1}</td>
                                <td>{val.sub_category_name}</td>
                                <td>{val.category.category_name}</td>
                                <td>
                                    <Link to={`/sub-categories/list/edit/${val._id}`}>Edit</Link>
                                    <button className="ml-2" onClick={
                                        () => {

                                            remove(val._id)
                                        }
                                    }>Delete</button>
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
    )
};


export default SubCategoriesList;


