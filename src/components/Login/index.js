import React from "react";


const Login = () => {
    
    return(


        <div className="row mx-md-n5" style={{"margin-top" : "100px"}}>
            <div className="col px-md-5" style={{"min-height" : "400px"}}>
                <div className="p-3 border bg-light">
                    <h1>Village Fres Admin</h1>    
                </div>
            </div>
            <div className="col px-md-5" style={{"min-height" : "400px"}}>
                <div className="p-3 border bg-light">
                    <form>
                        <div className="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" />
                        </div>
                        <div className="form-group form-check">
                            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
       
    )


};

export default Login;