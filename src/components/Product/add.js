import React,{useState,useEffect} from "react";
import axios from "axios";
import { SERVER_URL } from "../../config";

const AddProduct = () => {

    const [category,setCategory] = useState([]);
    const [subcategory,setsubCategory] = useState([]);
    const [category_changed_val,setChangedCategory] = useState("");
    const [sub_category_changed_val,setSubChangedCategory] = useState("");
    const [title,setTitle] = useState("");
    const [sku,setSku] = useState("");
    const [description,setDescription] = useState("");
    const [d_information,setDInformation] = useState("");
    const [base_price,setBPrice] = useState("");
    const [offer_price,setOPrice] = useState("");
    const [quantiy_available,setQuantity] = useState("");
    const [images,setImages] = useState([]);
    const [image_html,setImageHtml] = useState([]);


    useEffect(
        () => {

            async function callAjax() {

                var cat_response = await loadCategories();
                cat_response = cat_response.data;
                console.log(cat_response)
                if(cat_response.status === 1){
                    setCategory(cat_response.data);
                }
                
            }

            callAjax();

        },
        []
    )

    function loadCategories() {
        return  axios.get(SERVER_URL+"categories");

    }

    function loadSubCategories(cat_id) {

        return  axios.get(SERVER_URL+"categories/sub-categories/list/"+cat_id);

    }

 
    async function updateCategoryChangeValue(e) {

        setChangedCategory(e.target.value);
        var sub_cat_response = await loadSubCategories(e.target.value);
        sub_cat_response = sub_cat_response.data;
        if(sub_cat_response.status === 1){
            setsubCategory(sub_cat_response.data);

        }
    }

    function updateSubCategoryChangeValue(e) {
        setSubChangedCategory(e.target.value);
    }

    function updateImageContent(e){
        e.preventDefault();
        let cotnent_length = image_html.length + 1;
        console.log(cotnent_length)
        setImageHtml(image_html => [...image_html, cotnent_length]);

    }


    async function saveProduct(e) {
        e.preventDefault() // Stop form submit

        var formData = new FormData();

        if(images.length > 0) {
            images.map((val,index) => {
                formData.append('product_images',val);
            })
        }
        
        formData.append('title',title);
        formData.append('sku',sku);
        formData.append('description',description);
        formData.append('category',category_changed_val);
        formData.append('sub_category',sub_category_changed_val);
        formData.append('delivery_information',d_information);
        formData.append('quantiy_available',quantiy_available);
        formData.append('base_price',base_price);
        formData.append('offer_price',offer_price);
        const config = { headers: { 'Content-Type': 'multipart/form-data' } };

        var url = SERVER_URL+"products";
        var createproduct = await axios.post(url,formData,config);

        var createproduct_response = createproduct.data;
        if(createproduct_response.status === 1){
            alert("Successfully added");
        }

        console.log(createproduct)


        

        return false;
    }

    



    return(

        <div>
            <form onSubmit={saveProduct} action="#" method="post" encType="multipart/form-data">
            <div className="form-group">
                <label>Title <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Title" className="form-control"  value={title}
                        onChange={(e) => {
                            setTitle(e.target.value);
                        }}
                    />
            </div>

            <div className="form-group">
                <label>Sku</label>
                <input type="text" placeholder="Product SKU" className="form-control"  value={sku}
                        onChange={(e) => {
                            setSku(e.target.value);
                        }}
                    />
            </div>

            <div className="form-group">
                <label>Description</label>
                <textarea  placeholder="Product Description" className="form-control"  
                        onChange={(e) => {
                            setDescription(e.target.value);
                        }}
                        defaultValue={description}
                ></textarea>
            </div>



            <div className="form-group">
                <label>Category <span className="text-danger">*</span></label>
                <select className="form-control" onChange={updateCategoryChangeValue}>
                    <option>Select Category</option>
                    {
                        category && category.map((val,index) => {
                            return <option key={index} value={val._id}>{val.category_name}</option>
                        })
                    }
                </select>
            </div>
        
            <div className="form-group">
                <label>Sub Category <span className="text-danger">*</span></label>
                <select className="form-control" onChange={updateSubCategoryChangeValue}>
                    <option>Select Sub Category</option>
                    {
                        subcategory && subcategory.map((val,index) => {
                            return <option key={index} value={val._id}>{val.sub_category_name}</option>
                        })
                    }
                </select>
            </div>



            <div className="form-group">
                <label>Delivery Information</label>
                <input type="text" placeholder="Product Delivery Information" className="form-control"  value={d_information}
                        onChange={(e) => {
                            setDInformation(e.target.value);
                        }}
                    />
            </div>


            <div className="form-group">
                <label>Base Price <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Base Price" className="form-control"  value={base_price}
                        onChange={(e) => {
                            setBPrice(e.target.value);
                        }}
                    />
            </div>




            <div className="form-group">
                <label>Offer Price <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Offer Price" className="form-control"  value={offer_price}
                        onChange={(e) => {
                            setOPrice(e.target.value);
                        }}
                    />
            </div>

            <div className="form-group">
                <label>Quantity Available <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Quantity" className="form-control"  value={quantiy_available}
                        onChange={(e) => {
                            setQuantity(e.target.value);
                        }}
                    />
            </div>

            <div className="form-group">
                <label>Images</label>
                <input type="file" className="form-control" 
                        onChange={(e) => {
                            images.push(e.target.files[0]);
                                        setImages(images);
                        }}
                    />
                
            </div>

            <div className="form-group">
            <label></label>
            <button onClick={updateImageContent}>Add More</button>
            </div>

            {
                image_html && image_html.map((val,index) => {
                    return (
                        <div className="form-group" key={val}>
                            <label>Images</label>
                            <input type="file" className="form-control" 
                                    onChange={(e) => {
                                        images.push(e.target.files[0]);
                                        setImages(images);
                                    }}
                                />
                            
                            <button className="btn btn-danger" onClick={(event) => {

                                        event.preventDefault();
                                        var new_image_html = image_html.filter( (img) => {
                                                if(img !== val) return img; 
                                        });
                                        setImageHtml(new_image_html);

                            }}>X</button>
                        </div>
                    )
                 }
                )
            }

          

            <button className="btn btn-primary" type="submit" >Save</button>
            </form>
    </div>

    );
};

export default AddProduct;