import React , { useEffect , useState }from "react";
import axios from "axios";
import { SERVER_URL } from "../../config";
import { Link } from "react-router-dom";


const ProductList = () => {
    const [products, setProducts] = useState([]);

    useEffect(
        () => {
            
            callAjax()
        },
        []
    );

    async function callAjax() {

        var products_response = await loadProducts();
        products_response = products_response.data;
        if(products_response.status === 1){
            setProducts(products_response.data);
        }

    }

    function loadProducts() {

        return axios.get(SERVER_URL+"products");
    }

    const removeProduct = async (id) => {
        var confirm = window.confirm("Are you sure wanted to remove?");

        if(confirm) {

            var remove_product = await axios.delete(`${SERVER_URL}products/${id}`);
            remove_product = remove_product.data;
            if(remove_product.status === 1){
                callAjax();
            }
        }    
    };

    return (

        <div>
            <h1>Product List</h1>

            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products && products.map( (val,index) => {
                            return(
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{val.title}</td>
                                    <td>
                                        <Link className="btn btn-secondary" to={`/products/list/view/${val._id}`} >Detail</Link>
                                        <Link className="btn btn-secondary ml-2" to={`/products/list/edit/${val._id}`} >Edit</Link>
                                        <button className="ml-2 btn btn-secondary" 
                                        onClick={() => {

                                            removeProduct(val._id);
                                        }}
                                        >Delete</button>

                                    </td>
                                </tr>
                            )
                        })
                        }
                </tbody>
            </table>
        </div>
    )


};


export default ProductList;