import React, { useEffect, useState } from "react";
import {useParams,Link} from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "../../config";


const ProductDetail = () => {

    const { productid } = useParams();
    let initialState = {
        title : "",
        sku : "",
        description : "",
        images : [],
        category : "",
        sub_category : "",
        base_price : "",
        offer_price : "",
        quantiy_available : "",
        delivery_information: ""
    };
    const [product,setProducts]= useState(initialState);
    useEffect(
        () => {

            async function callAjax() {
                var product_response = await loadProductDetails(productid);
                product_response = product_response.data;
                if(product_response.status === 1){
                    initialState = {
                        title : product_response.data.title,
                        sku : product_response.data.sku,
                        description : product_response.data.description,
                        base_price : product_response.data.base_price,
                        offer_price : product_response.data.offer_price,
                        quantiy_available : product_response.data.quantiy_available,
                        category : product_response.data.category.category_name,
                        sub_category : product_response.data.sub_category.sub_category_name,
                        delivery_information : product_response.data.delivery_information
                    };

                    if(product_response.data.images){
                        var data = []
                        product_response.data.images.map(val => {
                            data.push(val)
                        });
                        initialState.images = data;
                    }

                    console.log(initialState.images)

                    setProducts(initialState);
                }
            }
            callAjax();
        },
        []
    );

    function loadProductDetails(productid) {
        return axios.get(SERVER_URL+"products/"+productid);
    }

    return (

        <div>
          
            <div className="row card">
                <div className="col-md-12">
                    <div className="card-header">
                        <h3>{product.title}</h3>
                        <Link to="/products/list">Product List</Link>
                    </div>

                    <div className="card card-body">

                            <p className="card card-body bg-light"><b>Sku</b> <br/> {product.sku} </p>
                            <p className="card card-body bg-light"><b>Description</b> <br/>  {product.description}</p>
                            <p className="card card-body bg-light"><b>Category</b> <br/>  {product.category}</p>
                            <p className="card card-body bg-light"><b>Sub Category</b> <br/>  {product.sub_category}</p>                
                            <p className="card card-body bg-light"><b>Base Price</b> <br/> INR {product.base_price}</p>
                            <p className="card card-body bg-light"><b>Offer Price</b> <br/> INR {product.offer_price}</p>
                            <p className="card card-body bg-light"><b>Available Quantity</b> <br/> INR {product.quantiy_available}</p>
                            <p className="card card-body bg-light"><b>Delivery Information</b> <br/> {product.delivery_information}</p>                    
                            <p className="card card-body bg-light"><b>Images</b> <br />
                                {product.images && product.images.map((val,index) => {

                                    var name = SERVER_URL+val.image_folder+"/"+ val.image_name;
                                    return(
                                    <span key={index}>
                                        <img src={name}  alt="Product Images"/> <br />
                                    </span>
                                    )
                                })}
                            </p>

                    </div>
                  
                </div>

        
            </div>
        </div>
    )


};

export default ProductDetail;