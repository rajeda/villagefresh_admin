import React from 'react';
import { Link  } from "react-router-dom";

 
const Navbar = () => {

    return(

      <div>
        <ul>
            <li>
              <Link to="/">Home</Link>
               
            </li>
            <li>
              <Link to="/categories">Categories</Link>
                <ul>
                  <li>
                    <Link to="/categories/add">Add</Link>
                  </li>
              
                  <li>
                    <Link to="/categories/list">list</Link>
                  </li>
                </ul>
            </li>

            <li>
              <Link to="/categories">Sub Categories</Link>
                <ul>
                  <li>
                    <Link to="/sub-categories/add">Add</Link>
                  </li>
            
                  <li>
                    <Link to="/sub-categories/list">list</Link>
                  </li>
                </ul>
            </li>

            <li>
              <Link to="/products">Products</Link>
                <ul>
                  <li>
                    <Link to="/products/add">Add</Link>
                  </li>
                  <li>
                    <Link to="/products/list">list</Link>
                  </li>
                </ul>
            </li>


          </ul>
      </div>
    );

};

export default Navbar;