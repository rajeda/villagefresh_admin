import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";


//component
import Navbar from "../Navbar";
import Home from "../Home";
import Category from "../Category";
import AddForm from '../Category/addForm';
import CategoriesList from '../Category/list';
import SubCategory from '../SubCategories';
import AddSubCategoryForm from '../SubCategories/addSubCategoryForm';
import SubCategoriesList from '../SubCategories/list';
import AddProduct from '../Product/add';
import ProductList from '../Product/list';
import ProductDetail from '../Product/detail';
import EditProduct from '../Product/edit';
import EditCategoryForm from '../Category/edit';
import EditSubCategoryForm from '../SubCategories/edit';



const routes = [
  {
    path: "/products/list/edit/:productid",
    component : EditProduct
  },
  {
    path: "/products/list/view/:productid",
    component : ProductDetail
  },
  {
    path: "/products/add",
    component : AddProduct
  },
  {
    path: "/products/list",
    component : ProductList
  },
  {
    path: "/categories/list/edit/:cat_id",
    component : EditCategoryForm
  },
  {
    path: "/categories/list/view/:cat_id",
    component : EditCategoryForm
  },
  {
    path: "/categories/list",
    component : CategoriesList
  },
  {
    path: "/categories/add",
    component : AddForm
  },
  {
    path: "/categories",
    component: Category
  },
  {
    path: "/sub-categories/add",
    component: AddSubCategoryForm
  },
  {
    path: "/sub-categories/list/edit/:sub_cat_id",
    component : EditSubCategoryForm
  },
  {
    path: "/sub-categories/list",
    component: SubCategoriesList
  },
  {
    path: "/sub-categories",
    component: SubCategory
  },
  {
    path: "/",
    component: Home
  },
];
function RouteWithSubRoutes(route) {
    return (
      <Route
        path={route.path}
        render={props => (
          // pass the sub-routes down to keep nesting
          <route.component {...props} routes={route.routes} />
        )}
      />
    );
  }

const Dashboard = () => {

    return(
        <Router>
            <div className="container">
                <h1>Admin</h1>
                <div className="row ">
                
                <div className="col-md-3 border border-secondary">
                    <Navbar />
                </div>
                <div className="col-md-9 border border-primary">
            
                    <Switch >
                    {routes.map((route, i) => (
                        <RouteWithSubRoutes key={i} {...route} />
                    ))}
                    </Switch>
                </div>
                </div>

            </div>
        </Router>

    )
}

export default Dashboard;