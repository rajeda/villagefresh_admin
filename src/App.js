import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import Login from './components/Login';
import Dashboard from './components/Dashboard';


const routes = [
  {
    path : "/sign-in",
    component : Login
  },
  {
    path : "/dashboard",
    component : Dashboard
  },
  {
    path: "/",
    component: Login
  },
];

function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}


function App() {
  return (
    <Router>
      <div className="container">
        <div className="row ">
          
          <div className="col-md-12">
    
            <Switch >
            {routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route} />
              ))}
            </Switch>


          </div>
        </div>

      </div>
    </Router>
  );
}

export default App;
